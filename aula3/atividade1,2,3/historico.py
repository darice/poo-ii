from datetime import datetime

class Historico:
    def __init__(self):
        self.dataAbertura = datetime.now()
        self.transacoes = []
    
    def imprime(self):
        print("Data de abertura:", self.dataAbertura)
        for t in self.transacoes:
            print("-", t)


